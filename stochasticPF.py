# THIS CODE CAN BE USED TO COMPUTE THE PARTITION FUNCTION FOR THE HARMONIC OSCILLATOR POTENTIAL 

# import all libraries

import numpy as np
import statistics 

# defining all the parameters 

nGrid = 200
nSto  = 6000
dx    = 0.06
beta  = 5.0
P     = 1500
eps   = beta/P 
xmin  = -0.5*(nGrid-1)*dx
xmax  = 0.5*(nGrid-1)*dx
sqrtdx = np.sqrt(dx)
sqrdx  = dx*dx 

nIterations = 50


# function to create grid 

def gridF():
    g = np.arange(xmin,xmax+dx,dx)
    return g


# generate stochastic wavefunction 

def sWF():
    swf = np.random.choice([-1.0/sqrtdx,1.0/sqrtdx],size=nGrid)
    return swf

#generate hamiltonian given the grid 

def hamiltonian(g):
    h = np.zeros([nGrid,nGrid])
    for i in range(nGrid):
        h[i,i] = 0.5*grid[i]**2 + 1.0/sqrdx 
        h[i,np.mod(i+1,nGrid)] = -0.5/sqrdx 
        h[i,np.mod(i-1,nGrid)] = -0.5/sqrdx 
    return h

# anaylytic expression for the partition function 

def analyticPF():
    dPF = 0.0
    for i in range(1000):
        dPF += np.exp(-beta*(i+0.5))
    return dPF

# deterministic partition function evaluated numerically 

def detPF(hamil):
    dPF = 0.0
    eVals, eVecs = np.linalg.eigh(hamil)
    for i in range(len(eVals)):
        dPF += np.exp(-beta*eVals[i])
    return dPF


# action of the approximated boltzmann operator on a given wavefunction 
# e^{-beta H} \approx (1 - beta H) is used here 


def actH(h, wf):
    wfOut = np.dot(I-eps*h, wf)
    return wfOut


# generate the approximate form for the boltzmann operator 
# (1 - eps H) ^ P 

def approxBoltzmann(h):
    aBoltz = np.zeros([nGrid,nGrid])
    m = I-eps*h
    aBoltz = np.linalg.matrix_power(m, P)
    return aBoltz


print(analyticPF())

grid = gridF()
h = hamiltonian(grid)

print(detPF(h))

# defining identity matrix 

I = np.eye(nGrid)

# creating the approximate Boltzmann operator 

aBoltz = approxBoltzmann(h)

# array to store the computed values of the partition functions 

valList = np.zeros(nIterations)



for n in range(nIterations):
    StoPF = 0.0                                 # value of stochastic partition function
    for i in range(nSto):
        StoWF = sWF()                           # generating stochastic orbital 
        tmpWF = StoWF                           # creating a copy for use later
        StoWF = np.dot(aBoltz,tmpWF)            # acting approximate Boltzmann on it
        StoPF += np.dot(StoWF,tmpWF)*dx         # computing stochastic PF 
    valList[n] = (StoPF/nSto)                   # storing it in an array 
        
print("mean", np.mean(valList))
print("stdDev", statistics.stdev(valList))



